# -*- coding: utf-8 -*-
# Copyright (c) 2021 Federico Ruiz-Ugalde
# Author: Federico Ruiz Ugalde <memeruiz at gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
from datetime import datetime as dt
from enum import Enum, Flag, IntEnum, IntFlag
from time import sleep
import RPi.GPIO as gpio
gpio.setmode(gpio.BCM)


class Ps_state(Enum):
    ON = gpio.LOW
    OFF = gpio.HIGH


class Cmded_state(Enum):
    ON = gpio.HIGH
    OFF = gpio.LOW


class Power_supply(object):
    def __init__(self, name, rc_pin, dc_ok_pin, description=""):
        self.rc_pin = rc_pin
        self.dc_ok_pin = dc_ok_pin
        self.name = name
        self.description = description
        self.ps_state = {
            'state': None,
            'TS': None
        }
        self.cmded_state = {
            'state': None,
            'TS': None
        }
        gpio.setup(self.rc_pin, gpio.OUT, initial=gpio.HIGH)  # High is on
        gpio.setup(self.dc_ok_pin, gpio.IN)

    def update_ps_state(self):
        self.ps_state['TS'] = dt.now()
        self.ps_state['state'] = Ps_state(gpio.input(self.dc_ok_pin))

    def set_cmded_state(self, cmded_state_enum):
        self.cmded_state['TS'] = dt.now()
        self.cmded_state['state'] = cmded_state_enum

    def write_cmded_state(self):
        gpio.output(self.rc_pin, self.cmded_state['state'].value)

    def on(self):
        self.set_cmded_state(Cmded_state.ON)
        self.write_cmded_state()

    def off(self):
        self.set_cmded_state(Cmded_state.OFF)
        self.write_cmded_state()

    def get_state(self):
        if self.ps_state['state'] == Ps_state.ON:
            return(True)
        else:
            return(False)

    def get_ps_state(self):
        return(self.ps_state)


class Power_supplies(object):
    def __init__(self):
        self.power_supplies = {}

    def add(self, power_supply):
        self.power_supplies[power_supply.name] = power_supply

    def update_all_ps_states(self):
        for key in self.power_supplies:
            self.power_supplies[key].update_ps_state()

    def get_all_ps_states(self):
        out = []
        for key in self.power_supplies:
            self.power_supplies[key].update_ps_state()
            out.append((key, self.power_supplies[key].get_ps_state()))
        return(out)

    def set_all_cmded_states(self, key_state_tuple_list):
        for key, state in key_state_tuple_list:
            self.power_supplies[key].set_cmded_state(state)
            self.power_supplies[key].write_cmded_state()

    def get_ps(self, name):
        return(self.power_supplies[name])

    def __del__(self):
        gpio.cleanup()
