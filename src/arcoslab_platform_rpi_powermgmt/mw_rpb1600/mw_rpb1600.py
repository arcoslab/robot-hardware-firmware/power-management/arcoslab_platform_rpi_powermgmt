# -*- coding: utf-8 -*-
# Copyright (c) 2021 Federico Ruiz-Ugalde
# Author: Federico Ruiz Ugalde <memeruiz at gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import smbus
from python_robot_misc_utils.binary_utils import floating_point_interpreter, flag_state, binlist_to_string
import errno
from datetime import datetime as dt
from enum import Enum, Flag, IntEnum, IntFlag
from time import sleep
from pprint import pprint as pp
import RPi.GPIO as gpio
gpio.setmode(gpio.BCM)


def update_and_print_vars_stats(ps, endl='\n'):
    ps.update_variables()
    ps.update_status()
    print(
        (
            f"Vin: {ps.variables['vin']['data']:8.4f}V, "
            f"Vout: {ps.variables['vout']['data']:7.4f}V, "
            f"Iout: {ps.variables['iout']['data']:7.4f}A, "
            f"fan0: {ps.variables['fans']['data'][0]:5d}rpm, "
            f"fan1: {ps.variables['fans']['data'][1]:5d}rpm, "
            f"chg_stat: {ps.status['chg']['data']:18}"
            f"chg_stat: {ps.status['word']['data']}"
        ), end=endl
    )


psprint = update_and_print_vars_stats


class Ps_state(Enum):
    """DC-OK signal"""
    ON = gpio.LOW  # PS ON!
    OFF = gpio.HIGH  # PS OFF!


class Ps_t_alarm_state(Enum):
    """Internal Temperature and FAN problems"""
    OK = gpio.LOW  # All fine!
    FAIL = gpio.HIGH  # Internal temperature too high, or FAN fails


class Cmded_state(Enum):
    """Remote signal for ON-OFF"""
    ON = gpio.HIGH
    OFF = gpio.LOW


class D0(Enum):
    CHG_CURVE = 0
    PMBUS_PVPC_SVR = 1


class Mw_rpb1600(object):
    REG_OPERATION = (0x01, 1)
    REG_ON_OFF_CONFIG = (0x02, 1)
    REG_CAPABILITY = (0x19, 1)
    REG_VOUT_MODE = (0x20, 1)
    REG_VOUT_COMMAND = (0x21, 2)
    REG_VOUT_TRIM = (0x22, 2)
    REG_IOUT_OC_FAULT_LIMIT = (0x46, 2)
    REG_IOUT_OC_FAULT_RESPONSE = (0x47, 1)
    REG_STATUS_WORD = (0x79, 2)
    REG_STATUS_VOUT = (0x7A, 1)
    REG_STATUS_IOUT = (0x7B, 1)
    REG_STATUS_INPUT = (0x7C, 1)
    REG_STATUS_TEMPERATURE = (0x7D, 1)
    REG_STATUS_CML = (0x7E, 1)
    REG_STATUS_MFR_SPECIFIC = (0x80, 1)
    REG_STATUS_FANS_1_2 = (0x81, 1)
    REG_READ_VIN = (0x88, 2)
    REG_READ_VOUT = (0x8B, 2)
    REG_READ_IOUT = (0x8C, 2)
    REG_READ_FAN_SPEED_1 = (0x90, 2)
    REG_READ_FAN_SPEED_2 = (0x91, 2)
    REG_PMBUS_REVISION = (0x98, 1)
    REG_MFR_ID = (0x99, 12)
    REG_MFR_MODEL = (0x9A, 12)
    REG_MFR_REVISION = (0x9B, 6)
    REG_MFR_LOCATION = (0x9C, 3)
    REG_MFR_DATE = (0x9D, 6)
    REG_MFR_SERIAL = (0x9E, 12)
    REG_CURVE_ICHG = (0xB0, 2)
    REG_CURVE_VBST = (0xB1, 2)
    REG_CURVE_VFLOAT = (0xB2, 2)
    REG_CURVE_ITAPER = (0xB3, 2)
    REG_CURVE_CONFIG = (0xB4, 2)
    REG_CURVE_CC_TIMEOUT = (0xB5, 2)
    REG_CURVE_CV_TIMEOUT = (0xB6, 2)
    REG_CURVE_FLOAT_TIMEOUT = (0xB7, 2)
    REG_CHG_STATUS = (0xB8, 2)

    class _Vout_mode(IntEnum):
        Linear = 0b000
        VID = 0b001
        Direct = 0b010

    class Operation(IntFlag):
        OFF = 0x00
        ON = 0x80
        SOFT_OFF = 0x40
        MARGIN_HIGH = 0x20
        MARGIN_LOW = 0x10
        ACT_ON_FAULT = 0x08
        ING_FAULT = 0x04

    class On_off(Flag):
        POWER_UP_REQ_CONTROL_PIN_AND_OPERATION = (1 << 4)
        POWER_UP_REQ_OPERATION_ON = (1 << 3)
        POWER_UP_REQ_CONTROL_PIN = (1 << 2)
        CONTROL_PIN_POL = (1 << 1)
        POWER_OFF_NO_DELAY = (1 << 0)

    class Capability(Flag):
        PACKET_ERR_CHK = (1 << 7)
        MAX_BUS_RESERVED = (1 << 6)
        MAX_BUS_SPK_400KH = (1 << 5)
        SMBALERT = (1 << 4)
        RESERVED3 = (1 << 3)
        RESERVED2 = (1 << 2)
        RESERVED1 = (1 << 1)
        RESERVED0 = (1 << 0)

    class Iout_oc_fault_response(Flag):
        PMBUS_ON_CURRENT_FAULT_LIMIT = 0b00
        PMBUS_ON_CURRENT_FAULT_LIMIT_WITH_OC_UV_FAULT_LIMIT = 0b01
        PMBUS_ON_CURRENT_FAULT_LIMIT_WITH_DELAY_RETRY = 0b10
        PMBUS_OFF_RETRY = 0b11

    class Curve_config(IntFlag):
        FTTOE = 0x0400
        CVTOE = 0x0200
        CCTOE = 0x0100
        STGS = 0x0040
        NULL = 0x0000

    class Curve_config_tcs(IntEnum):
        DISABLED = 0b00
        TC_3MV_C_CELL = 0b01
        TC_4MV_C_CELL = 0b10
        TC_5MV_C_CELL = 0b11

    class Curve_config_cuvs(IntEnum):
        CUSTOM = 0b00
        GEL = 0b01
        FLOODED = 0b10
        AGM = 0b11

    class Status_word(Flag):
        VOUT = 1 << (7+8)
        IOUT_POUT = 1 << (6+8)
        INPUT = 1 << (5+8)
        MFR = 1 << (4+8)
        POWER_GOOD_NEG = 1 << (3+8)
        FANS = 1 << (2+8)
        OTHER = 1 << (1+8)
        UNKNOWN = 1 << 8
        BUSY = 1 << (7)
        OFF = 1 << (6)
        VOUT_OV = 1 << 5
        IOUT_OC = 1 << (4)
        VIN_UV = 1 << (3)
        TEMPERATURE = 1 << (2)
        CML = 1 << (1)
        NONE_7_1 = 1 << (0)

    class Status_vout(Flag):
        OV_FAULT = 1 << 7
        OV_WARN = 1 << 6
        UV_WARN = 1 << 5
        UV_FAULT = 1 << 4
        MAX_WARN = 1 << 3
        TON_MAX_FAULT = 1 << 2
        TOFF_MAX_WARN = 1 << 1
        TRACK_ERR = 1 << 0

    class Status_iout(Flag):
        OC_FAULT = 1 << 7
        OC_LOWV_SHUT_FAULT = 1 << 6
        OC_WARN = 1 << 5
        UC_FAULT = 1 << 4
        SHARE_FAULT = 1 << 3
        POWER_LIM = 1 << 2
        POUT_OP_FAULT = 1 << 1
        POUT_OP_WARN = 1 << 0

    class Status_input(Flag):
        OV_FAULT = 1 << 7
        OV_WARN = 1 << 6
        UV_WARN = 1 << 5
        UV_FAULT = 1 << 4
        OFF_LOW_INPUT_VOLT = 1 << 3
        IIN_OC_FAULT = 1 << 2
        IIN_OC_WARN = 1 << 1
        PIN_OP_WARN = 1 << 0

    class Status_temperature(Flag):
        OT_FAULT = 1 << 7
        OT_WARN = 1 << 6
        UT_WARN = 1 << 5
        UT_FAULT = 1 << 4
        RESERVED3 = 1 << 3
        RESERVED2 = 1 << 2
        RESERVED1 = 1 << 1
        RESERVED0 = 1 << 0

    class Status_cml(Flag):
        INV_UNS_CMD_REC = 1 << 7
        INV_UNS_DAT_REC = 1 << 6
        PACKET_ERR_CHK_FAILED = 1 << 5
        MEM_FAULT = 1 << 4
        PROC_FAULT = 1 << 3
        RESERVED = 1 << 2
        OTHER_COMM_FAULT = 1 << 1
        OTHER_MEM_LOG_FAULT = 1 << 0

    class Status_mfr_specific(Flag):
        MAN_DEF_7 = 1 << 7
        MAN_DEF_6 = 1 << 6
        MAN_DEF_5 = 1 << 5
        MAN_DEF_4 = 1 << 4
        MAN_DEF_3 = 1 << 3
        MAN_DEF_2 = 1 << 2
        MAN_DEF_1 = 1 << 1
        MAN_DEF_0 = 1 << 0

    class Status_fans(Flag):
        FAN1_FAULT = 1 << 7
        FAN2_FAULT = 1 << 6
        FAN1_WARN = 1 << 5
        FAN2_WARN = 1 << 4
        FAN1_SPD_OR = 1 << 3
        FAN2_SPD_OR = 1 << 2
        AF_FAULT = 1 << 1
        AF_WARN = 1 << 0

    class Status_chg(Flag):
        FTTOF = 1 << (7+8)
        CVTOF = 1 << (6+8)
        CCTOF = 1 << (5+8)
        BTNC = 1 << (3+8)
        NTCER = 1 << (2+8)
        EEPER = 1 << (0+8)
        FVM = 1 << (3)
        CVM = 1 << (2)
        CCM = 1 << (1)
        FULLM = 1 << (0)

    def __init__(self, address=0x47, n_smbus=1, d0=D0.CHG_CURVE, remote_pin=None, t_alarm_pin=None, dc_ok_pin=None):
        ''' address: I2C bus address. Default 0x47
        n_smbus: RPI bus connected to the RPB1600
        By default the only free smbus is the number 1'''
        self.address = address
        self.d0 = d0
        self.remote_pin = remote_pin
        if remote_pin:
            print("Using remote_pin")
            gpio.setup(self.remote_pin, gpio.OUT, initial=gpio.LOW)  # OFF
        self.t_alarm_pin = t_alarm_pin
        if t_alarm_pin:
            print("Using t_alarm_pin")
            gpio.setup(self.t_alarm_pin, gpio.IN)
        self.dc_ok_pin = dc_ok_pin
        if dc_ok_pin:
            print("Using dc_ok_pin")
            gpio.setup(self.dc_ok_pin, gpio.IN)

        # datastructures
        self.mfr_info = {}
        self.status = {
            'conn_established': False
        }
        self.variables = {}
        self.config = {}

        self.bus = smbus.SMBus(n_smbus)
        self.fp_converter = floating_point_interpreter()
        self._init_config()
        self._init_status()
        self._init_variables()
        self._init_pin_data()

    def _init_pin_data(self):
        self.pin_data = {
            'TS': None,
            'ps_state': None,
            'ps_t_alarm_state': None,
            'cmded_state': None
        }

    def _init_config(self):
        self._init_operation()
        self._init_on_off_config()
        self._init_capability()
        self._init_vout_mode()
        self._init_vout_command()
        self._init_vout_trim()
        self._init_iout_oc_fault_limit()
        self._init_iout_oc_fault_response()
        self._init_curve_cc_timeout()
        self._init_curve_cv_timeout()
        self._init_curve_ichg()
        self._init_curve_vbst()
        self._init_curve_vfloat()
        self._init_curve_itaper()
        self._init_curve_config()
        self._init_curve_float_timeout()

    def _init_status(self):
        self._init_status_word()
        self._init_status_vout()
        self._init_status_iout()
        self._init_status_input()
        self._init_status_temperature()
        self._init_status_cml()
        self._init_status_mfr_specific()
        self._init_status_fans()
        self._init_chg_status()

    def _init_variables(self):
        self._init_vin()
        self._init_vout()
        self._init_iout()
        self._init_fan_speeds()

    def read_smbus(self, register, length=1):
        sleep(0.002)
        try:
            if length == 1:
                data = self.bus.read_byte_data(self.address, register)
            elif length == 2:
                data = self.bus.read_word_data(self.address, register)
            else:
                data = self.bus.read_i2c_block_data(
                    self.address, register, length)
        except OSError as e:
            if e.errno == 121:
                print("Error: I2C communication error. Connection lost")
                self.status['conn_established'] = False
            raise
        else:
            if ((register != self.REG_STATUS_WORD[0])
                    and (register != self.REG_STATUS_CML[0])):
                #print(f'Register: 0x{register:x}')
                #print("raw data", data)
                self.update_status_word()
                if self.status['word']['data'] == self.Status_word.CML:
                    print("Error: SMBus communication error: CML flag")
                    # self.status['conn_established'] = False
                    # raise OSError(errno.EREMOTEIO, "SMBus I/O error")
                    return(data)
                else:
                    old_conn_status = self.status['conn_established']
                    self.status['conn_established'] = True
                    if old_conn_status != self.status['conn_established']:
                        print("Connection established")
                    return(data)
            else:
                old_conn_status = self.status['conn_established']
                self.status['conn_established'] = True
                if old_conn_status != self.status['conn_established']:
                    print("Connection established")
                return(data)

    def update_pin_data(self):
        self.pin_data['TS'] = dt.now()
        if self.t_alarm_pin:
            self.pin_data['ps_t_alarm_state'] = Ps_t_alarm_state(
                gpio.input(self.t_alarm_pin))
        if self.dc_ok_pin:
            self.pin_data['ps_state'] = Ps_state(
                gpio.input(self.dc_ok_pin))

    def set_pin_data(self, key, cmded_state_enum):
        """Sets the datastructure cmded pin state"""
        if self.remote_pin:
            self.pin_data[key] = cmded_state_enum

    def write_pin_data(self):
        if self.remote_pin:
            gpio.output(self.remote_pin, self.pin_data['cmded_state'].value)

    def write_smbus(self, register, length, data):
        sleep(0.005)
        if length == 1:
            return(self.bus.write_byte_data(self.address, register, data))
        elif length == 2:
            return(self.bus.write_word_data(self.address, register, data))
        else:
            return(self.bus.write_i2c_block_data(self.address, register, data))

    def _check_write(self, register, length, w_value):
        res = self.read_smbus(register, length)
        if res == w_value:
            #print("Write successful!")
            pass
        else:
            print("Problem during write")

    def _check_write_result(self, read_value, write_value):
        if read_value == write_value:
            pass
            #print("Write successful!")
        else:
            print("Problem during write")

    def _init_operation(self):
        self.config['operation'] = {
            'Description': "Operation ON-OFF RW register",
            'write': True,
            'TS': None,
            'data': None,
            'data_w': None
        }

    def update_operation(self):
        res = self.read_smbus(*self.REG_OPERATION)
        self.config['operation']['TS'] = dt.now()
        self.config['operation']['data'] = self.Operation(res)

    def set_operation(self, operation):
        self.config['operation']['data_w'] = operation

    def write_operation(self):
        dout = int(self.config['operation']['data_w'].value)
        self.write_smbus(*self.REG_OPERATION, dout)
        self.update_operation()
        self._check_write_result(self.config['operation']['data'].value, dout)

    def _init_on_off_config(self):
        self.config['on_off'] = {
            'Description': "ON OFF Configuration read-only register",
            'write': False,
            'TS': None,
            'data': None
        }

    def update_on_off_config(self):
        res = self.read_smbus(*self.REG_ON_OFF_CONFIG)
        self.config['on_off']['TS'] = dt.now()
        self.config['on_off']['data'] = self.On_off(res)

    def _init_capability(self):
        self.config['capability'] = {
            'Description': "Power supply capabilities",
            'write': False,
            'TS': None,
            'data': None
        }

    def update_capability(self):
        res = self.read_smbus(*self.REG_CAPABILITY)
        self.config['capability']['TS'] = dt.now()
        self.config['capability']['data'] = self.Capability(res)

    def _init_vout_mode(self):
        self.config['vout_mode'] = {
            'Description': "Vout Mode configuration register",
            'write': False,
            'TS': None,
            'data': {
                'mode': None,
                'mode_param': None
            }
        }

    def update_vout_mode(self):
        '''Read Vout numerical mode'''
        mode_pos = 5
        # PMBus specs Part II rev 1-1 Page 24
        vout_mode_raw = self.read_smbus(*self.REG_VOUT_MODE)
        vout_mode = self._Vout_mode(vout_mode_raw >> mode_pos)
        #print("VOUT_MODE: ", vout_mode)
        vout_mode_param = vout_mode_raw & ~ (7 << mode_pos)
        if (vout_mode_param >> mode_pos-1) > 0:
            #print("Negative. Extending sign bits")
            vout_mode_param = vout_mode_param | (~ 0x1F)
        #print("VOUT_MODE parameter: ", vout_mode_param)
        self.config['vout_mode']['TS'] = dt.now()
        self.config['vout_mode']['data']['mode'] = vout_mode
        self.config['vout_mode']['data']['mode_param'] = vout_mode_param

    def _init_vout_command(self):
        self.config['vout_command'] = {
            'Description': "Vout command reference voltage (Read-only)",
            'write': False,
            'TS': None,
            'data': None
        }

    def update_vout_command(self):
        self.config['vout_command']['TS'] = dt.now()
        raw_data = self.read_smbus(*self.REG_VOUT_COMMAND)
        #print("Raw data: ", hex(raw_data), raw_data)
        assert (self.config['vout_mode']['data']['mode'] ==
                self._Vout_mode.Linear), "Only VOUT_MODE Linear implemented"
        if self.config['vout_mode']['data']['mode'] == self._Vout_mode.Linear:
            self.config['vout_command']['data'] = raw_data * \
                (2**(self.config['vout_mode']['data']['mode_param']))

    def _init_vout_trim(self):
        self.config['vout_trim'] = {
            'Description': "Vout Trim register",
            'write': True,
            'TS': None,
            'data': None,
            'data_w': None
        }

    def update_vout_trim(self):
        self.config['vout_trim']['TS'] = dt.now()
        raw_data = self.read_smbus(*self.REG_VOUT_TRIM)
        #print("Raw data: ", hex(raw_data), raw_data)
        assert (self.config['vout_mode']['data']['mode'] ==
                self._Vout_mode.Linear), "Only VOUT_MODE Linear implemented"
        if self.config['vout_mode']['data']['mode'] == self._Vout_mode.Linear:
            self.config['vout_trim']['data'] = raw_data * \
                (2**(self.config['vout_mode']['data']['mode_param']))
            # self.fp_converter.set_n_bits(5)
            # self.config['vout_trim']['data'] = self.fp_converter.calc_fp_value(
            #    raw_data)
        return(raw_data)

    def set_vout_trim(self, voltage):
        """ Set Vout Trim voltage on internal datastructure

        voltage: float (range: -12 - +12, units: volts)
        """
        self.config['vout_trim']['data_w'] = voltage

    def write_vout_trim(self):
        rawdata = int(self.config['vout_trim']['data_w'] /
                      (2**(self.config['vout_mode']['data']['mode_param'])))
        # self.fp_converter.set_n_bits(5)
        # rawdata = self.fp_converter.calc_bin_value(
        #    self.config['vout_trim']['data_w'], N)
        self.write_smbus(*self.REG_VOUT_TRIM, rawdata)
        read_raw_data = self.update_vout_trim()
        self._check_write_result(read_raw_data, rawdata)

    def _init_iout_oc_fault_limit(self):
        self.config['iout_oc_fault_limit'] = {
            'Description': "Iout overcurrent fault limit register",
            'write': True,
            'TS': None,
            'data': None,
            'data_w': None
        }

    def update_iout_oc_fault_limit(self):
        raw_data = self.read_smbus(*self.REG_IOUT_OC_FAULT_LIMIT)
        self.config['iout_oc_fault_limit']['TS'] = dt.now()
        #print("Raw data: ", hex(raw_data), raw_data)
        self.fp_converter.set_n_bits(5)
        res = self.fp_converter.calc_fp_value(raw_data)
        self.config['iout_oc_fault_limit']['data'] = res
        return(raw_data)

    def set_iout_oc_fault_limit(self, out_current_limit):
        """ Set Output Current Limit before fault on internal datastructure

        Current: float (units: Ampers)
        """
        self.config['iout_oc_fault_limit']['data_w'] = out_current_limit

    def write_iout_oc_fault_limit(self):
        self.fp_converter.set_n_bits(5)
        raw_data = self.fp_converter.calc_bin_value(
            self.config['iout_oc_fault_limit']['data_w'], -3)
        self.write_smbus(*self.REG_IOUT_OC_FAULT_LIMIT,  raw_data)
        read_raw_data = self.update_iout_oc_fault_limit()
        self._check_write_result(read_raw_data, raw_data)

    def _init_iout_oc_fault_response(self):
        self.config['iout_oc_fault_response'] = {
            'Description': "Iout overcurrent fault response register",
            'write': False,
            'TS': None,
            'data': {
                'res': None,
                'retry': None,
                'delay': None
            }
        }

    def update_iout_oc_fault_response(self):
        response_bit = 6
        retry_bit = 3
        retry_mask = 0b00111000
        delay_time_bit = 0
        delay_time_mask = 0b00000111
        res = self.read_smbus(*self.REG_IOUT_OC_FAULT_RESPONSE)
        #print("Raw data: ", hex(res), res)
        self.config['iout_oc_fault_response']['TS'] = dt.now()
        self.config['iout_oc_fault_response']['data'] = {
            'response': self.Iout_oc_fault_response(res >> response_bit),
            'retry': (res & retry_mask) >> retry_bit,
            'delay': delay_time_mask & res
        }

    def _init_status_word(self):
        self.status['word'] = {
            'Description': "Status Word",
            'write': False,
            'TS': None,
            'data': None
        }

    def update_status_word(self):
        res = self.read_smbus(*self.REG_STATUS_WORD)
        self.status['word']['TS'] = dt.now()
        self.status['word']['data'] = self.Status_word(res)

    def _init_status_vout(self):
        self.status['vout'] = {
            'Description': "Vout Status register",
            'write': False,
            'TS': None,
            'data': None
        }

    def update_status_vout(self):
        res = self.read_smbus(*self.REG_STATUS_VOUT)
        self.status['vout']['TS'] = dt.now()
        self.status['vout']['data'] = self.Status_vout(res)

    def _init_status_iout(self):
        self.status['iout'] = {
            'Description': "Iout Status register",
            'write': False,
            'TS': None,
            'data': None
        }

    def update_status_iout(self):
        res = self.read_smbus(*self.REG_STATUS_IOUT)
        self.status['iout']['TS'] = dt.now()
        self.status['iout']['data'] = self.Status_iout(res)

    def _init_status_input(self):
        self.status['input'] = {
            'Description': "Input Status register",
            'write': False,
            'TS': None,
            'data': None
        }

    def update_status_input(self):
        res = self.read_smbus(*self.REG_STATUS_INPUT)
        self.status['input']['TS'] = dt.now()
        self.status['input']['data'] = self.Status_input(res)

    def _init_status_temperature(self):
        self.status['temp'] = {
            'Description': "Temperature Status register",
            'write': False,
            'TS': None,
            'data': None
        }

    def update_status_temperature(self):
        res = self.read_smbus(*self.REG_STATUS_TEMPERATURE)
        self.status['temp']['TS'] = dt.now()
        self.status['temp']['data'] = self.Status_temperature(res)

    def _init_status_cml(self):
        self.status['cml'] = {
            'Description': "CML Status register",
            'write': False,
            'TS': None,
            'data': None
        }

    def update_status_cml(self):
        res = self.read_smbus(*self.REG_STATUS_CML)
        self.status['cml']['TS'] = dt.now()
        self.status['cml']['data'] = self.Status_cml(res)

    def _init_status_mfr_specific(self):
        self.status['mfr_specific'] = {
            'Description': "MFR specific Status register",
            'write': False,
            'TS': None,
            'data': None
        }

    def update_status_mfr_specific(self):
        res = self.read_smbus(*self.REG_STATUS_MFR_SPECIFIC)
        self.status['mfr_specific']['TS'] = dt.now()
        self.status['mfr_specific']['data'] = self.Status_mfr_specific(res)

    def _init_status_fans(self):
        self.status['fans'] = {
            'Description': "Fans Status register",
            'write': False,
            'TS': None,
            'data': None
        }

    def update_status_fans(self):
        res = self.read_smbus(*self.REG_STATUS_FANS_1_2)
        self.status['fans']['TS'] = dt.now()
        self.status['fans']['data'] = self.Status_fans(res)

    def _init_vin(self):
        self.variables['vin'] = {
            'Description': "Vin Variable",
            'write': False,
            'TS': None,
            'data': None
        }

    def update_vin(self):
        raw_data = self.read_smbus(*self.REG_READ_VIN)
        #print("Raw data: ", hex(raw_data), raw_data)
        self.variables['vin']['data'] = raw_data * (2**-1)

    def _init_vout(self):
        self.variables['vout'] = {
            'Description': "Vout Variable",
            'write': False,
            'TS': None,
            'data': None
        }

    def update_vout(self):
        raw_data = self.read_smbus(*self.REG_READ_VOUT)
        #print("Raw data: ", hex(raw_data), raw_data)
        assert (self.config['vout_mode']['data']['mode'] ==
                self._Vout_mode.Linear), "Only VOUT_MODE Linear implemented"
        if self.config['vout_mode']['data']['mode'] == self._Vout_mode.Linear:
            self.variables['vout']['data'] = raw_data * \
                (2**(self.config['vout_mode']['data']['mode_param']))

    def _init_iout(self):
        self.variables['iout'] = {
            'Description': "Iout Variable",
            'write': False,
            'TS': None,
            'data': None
        }

    def update_iout(self):
        raw_data = self.read_smbus(*self.REG_READ_IOUT)
        #print("Raw data: ", hex(raw_data), raw_data)
        self.fp_converter.set_n_bits(5)
        self.variables['iout']['data'] = self.fp_converter.calc_fp_value(
            raw_data)

    def _init_fan_speeds(self):
        self.variables['fans'] = {
            'Description': "Fan Speeds Variable",
            'write': False,
            'TS': None,
            'data': (None, None)
        }

    def update_fan_speeds(self):
        raw_data1 = self.read_smbus(*self.REG_READ_FAN_SPEED_1)
        #print("Raw data: ", hex(raw_data1), raw_data1)
        raw_data2 = self.read_smbus(*self.REG_READ_FAN_SPEED_2)
        #print("Raw data: ", hex(raw_data2), raw_data2)
        self.fp_converter.set_n_bits(5)
        self.variables['fans']['data'] = (self.fp_converter.calc_fp_value(
            raw_data1), self.fp_converter.calc_fp_value(raw_data2))

    def read_pmbus_revision(self):
        return(self.read_smbus(*self.REG_PMBUS_REVISION))

    def write_string_block(self, register, register_size, stringdata):
        """ Write an ascii string using a block I2C write cmd"""
        if len(stringdata) < register_size-1:
            print("Inserting spaces")
            stringdata += ' '*(register_size-1-len(stringdata))
            print("Extended stringdata: ", stringdata)
        elif len(stringdata) > register_size-1:
            print("Stringdata too long. Ignoring")
            return()
        self.write_smbus(register, register_size,
                         [register_size] + list(map(ord, stringdata)))

    def read_mfr_id(self):
        res = self.read_smbus(*self.REG_MFR_ID)
        return(binlist_to_string(res)[1:])

    def read_mfr_model(self):
        res = self.read_smbus(*self.REG_MFR_MODEL)
        return(binlist_to_string(res)[1:])

    def read_mfr_revision(self):
        res = self.read_smbus(*self.REG_MFR_REVISION)
        return(binlist_to_string(res)[1:])

    def read_mfr_location(self):
        res = self.read_smbus(* self.REG_MFR_LOCATION)
        return(binlist_to_string(res)[1:])

    def write_mfr_location(self, locationstring):
        """ Write an ascii location string to the power supply"""
        self.write_string_block(*self.REG_MFR_LOCATION, locationstring)

    def read_mfr_date(self):
        res = self.read_smbus(*self.REG_MFR_DATE)
        return(binlist_to_string(res)[1:])

    def write_mfr_date(self, datestring):
        """ Write an ascii date string to the power supply"""
        self.write_string_block(*self.REG_MFR_SERIAL, datestring)

    def read_mfr_serial(self):
        res = self.read_smbus(*self.REG_MFR_SERIAL)
        return(binlist_to_string(res)[1:])

    def write_mfr_serial(self, serial):
        """ Write an ascii serial number to the power supply"""
        self.write_string_block(*self.REG_MFR_SERIAL, serial)

    def _init_curve_cc_timeout(self):
        self.config['curve_cc_timeout'] = {
            'Description': "Curve CC timout register",
            'write': True,
            'TS': None,
            'data': None,
            'data_w': None
        }

    def update_curve_cc_timeout(self):
        res = self.read_smbus(*self.REG_CURVE_CC_TIMEOUT)
        self.config['curve_cc_timeout']['TS'] = dt.now()
        self.config['curve_cc_timeout']['data'] = res * (2**0)
        return(res)

    def set_curve_cc_timeout(self, time):
        """ Set curve cc timeout in internal datastructure

        time: int (from 60-64800, units: min)"""
        self.config['curve_cc_timeout']['data_w'] = time

    def write_curve_cc_timeout(self):
        rawdata = self.config['curve_cc_timeout']['data_w']
        self.write_smbus(*self.REG_CURVE_CC_TIMEOUT, rawdata)
        read_rawdata = self.update_curve_cc_timeout()
        self._check_write_result(read_rawdata, rawdata)

    def _init_curve_cv_timeout(self):
        self.config['curve_cv_timeout'] = {
            'Description': "Curve CV timout register",
            'write': True,
            'TS': None,
            'data': None,
            'data_w': None
        }
        pass

    def update_curve_cv_timeout(self):
        res = self.read_smbus(*self.REG_CURVE_CV_TIMEOUT)
        self.config['curve_cv_timeout']['TS'] = dt.now()
        self.config['curve_cv_timeout']['data'] = res * (2**0)
        return(res)

    def set_curve_cv_timeout(self, time):
        """ Set curve cv timeout in internal datastructure

        time: int (from 60-64800, units: min)"""
        self.config['curve_cv_timeout']['data_w'] = time

    def write_curve_cv_timeout(self):
        rawdata = self.config['curve_cv_timeout']['data_w']
        self.write_smbus(*self.REG_CURVE_CV_TIMEOUT, rawdata)
        read_rawdata = self.update_curve_cv_timeout()
        self._check_write_result(read_rawdata, rawdata)

    def _init_curve_ichg(self):
        self.config['curve_ichg'] = {
            'Description': "Curve Ichg register",
            'write': True,
            'TS': None,
            'data': None,
            'data_w': None
        }
        pass

    def update_curve_ichg(self):
        res = self.read_smbus(*self.REG_CURVE_ICHG)
        self.fp_converter.set_n_bits(5)
        data = self.fp_converter.calc_fp_value(res)
        self.config['curve_ichg']['TS'] = dt.now()
        self.config['curve_ichg']['data'] = data
        return(res)

    def set_curve_ichg(self, current):
        """ Set curve ichg in internal datastructure

        current: float (5.5A - 27.5A, units: amps)"""
        self.config['curve_ichg']['data_w'] = current

    def write_curve_ichg(self):
        self.fp_converter.set_n_bits(5)
        rawdata = self.fp_converter.calc_bin_value(
            self.config['curve_ichg']['data_w'], -3)
        self.write_smbus(*self.REG_CURVE_ICHG, rawdata)
        read_rawdata = self.update_curve_ichg()
        self._check_write_result(read_rawdata, rawdata)

    def _init_curve_vbst(self):
        self.config['curve_vbst'] = {
            'Description': "Curve Vbst register",
            'write': True,
            'TS': None,
            'data': None,
            'data_w': None
        }

    def update_curve_vbst(self):
        res = self.read_smbus(*self.REG_CURVE_VBST)
        data = res * (2**-9)
        self.config['curve_vbst']['TS'] = dt.now()
        self.config['curve_vbst']['data'] = data
        return(res)

    def set_curve_vbst(self, voltage):
        """ Set curve vbst in internal datastructure

        voltage: float (36-60V, units: volts)"""
        self.config['curve_vbst']['data_w'] = voltage

    def write_curve_vbst(self):
        rawdata = int(self.config['curve_vbst']['data_w']/(2**-9))
        self.write_smbus(*self.REG_CURVE_VBST, rawdata)
        read_rawdata = self.update_curve_vbst()
        self._check_write_result(read_rawdata, rawdata)

    def _init_curve_vfloat(self):
        self.config['curve_vfloat'] = {
            'Description': "Curve VFloat register",
            'write': True,
            'TS': None,
            'data': None,
            'data_w': None
        }
        pass

    def update_curve_vfloat(self):
        res = self.read_smbus(*self.REG_CURVE_VFLOAT)
        data = res * (2**-9)
        self.config['curve_vfloat']['TS'] = dt.now()
        self.config['curve_vfloat']['data'] = data
        return(res)

    def set_curve_vfloat(self, voltage):
        """ Set curve vfloat in internal datastructure

        voltage: float (36-vbst, units: volts)"""
        self.config['curve_vfloat']['data_w'] = voltage

    def write_curve_vfloat(self):
        rawdata = int(self.config['curve_vfloat']['data_w']/(2**-9))
        self.write_smbus(*self.REG_CURVE_VFLOAT, rawdata)
        read_rawdata = self.update_curve_vfloat()
        self._check_write_result(read_rawdata, rawdata)

    def _init_curve_itaper(self):
        self.config['curve_itaper'] = {
            'Description': "Curve itaper register",
            'write': True,
            'TS': None,
            'data': None,
            'data_w': None
        }
        pass

    def update_curve_itaper(self):
        res = self.read_smbus(*self.REG_CURVE_ITAPER)
        self.fp_converter.set_n_bits(5)
        data = self.fp_converter.calc_fp_value(res)
        self.config['curve_itaper']['TS'] = dt.now()
        self.config['curve_itaper']['data'] = data
        return(res)

    def set_curve_itaper(self, current):
        """ Set curve itaper in internal datastructure

        current: float (1.3 - 8.3A, units: Ampers)"""
        self.config['curve_itaper']['data_w'] = current

    def write_curve_itaper(self):
        self.fp_converter.set_n_bits(5)
        rawdata = self.fp_converter.calc_bin_value(
            self.config['curve_itaper']['data_w'], -3)
        self.write_smbus(*self.REG_CURVE_ITAPER, rawdata)
        read_rawdata = self.update_curve_itaper()
        self._check_write_result(read_rawdata, rawdata)

    def _init_curve_config(self):
        self.config['curve_config'] = {
            'Description': "Curve Config Register",
            'write': True,
            'TS': None,
            'data': {
                'flags': None,
                'tcs': None,
                'cuvs': None
            },
            'data_w': {
                'flags': None,
                'tcs': None,
                'cuvs': None
            }
        }

    def update_curve_config(self):
        flag_mask = 0b0000011101000000
        tcs_mask = 0b00001100
        tcs_pos = 2
        cuvs_mask = 0b00000011
        raw_data = self.read_smbus(*self.REG_CURVE_CONFIG)
        #print("curve config: ", hex(raw_data))
        self.config['curve_config']['TS'] = dt.now()
        self.config['curve_config']['data']['flags'] = self.Curve_config(
            flag_mask & raw_data)
        self.config['curve_config']['data']['tcs'] = self.Curve_config_tcs(
            (tcs_mask & raw_data) >> tcs_pos)
        self.config['curve_config']['data']['cuvs'] = self.Curve_config_cuvs(
            cuvs_mask & raw_data)
        return(raw_data)

    def set_curve_config(self, flags, tcs, cuvs):
        self.config['curve_config']['data_w']['flags'] = flags
        self.config['curve_config']['data_w']['tcs'] = tcs
        self.config['curve_config']['data_w']['cuvs'] = cuvs

    def write_curve_config(self):
        tcs_pos = 2
        rawdata = self.config['curve_config']['data_w']['flags'] | (
            self.config['curve_config']['data_w']['tcs'] << tcs_pos) | (self.config['curve_config']['data_w']['cuvs'])
        self.write_smbus(*self.REG_CURVE_CONFIG, rawdata)
        read_rawdate = self.update_curve_config()
        self._check_write_result(rawdata, read_rawdate)

    def _init_curve_float_timeout(self):
        self.config['curve_float_timeout'] = {
            'Description': "Curve Float Timeout register",
            'write': True,
            'TS': None,
            'data': None,
            'data_w': None
        }
        pass

    def update_curve_float_timeout(self):
        res = self.read_smbus(*self.REG_CURVE_FLOAT_TIMEOUT)
        data = res * (2**0)
        self.config['curve_float_timeout']['TS'] = dt.now()
        self.config['curve_float_timeout']['data'] = data
        return(res)

    def set_curve_float_timeout(self, time):
        """ Set curve float_timeout in internal datastructure

        time: float (60 - 64800, units: min)"""
        self.config['curve_float_timeout']['data_w'] = time

    def write_curve_float_timeout(self):
        rawdata = self.config['curve_float_timeout']['data_w']
        self.write_smbus(*self.REG_CURVE_FLOAT_TIMEOUT, rawdata)
        read_rawdata = self.update_curve_float_timeout()
        self._check_write_result(read_rawdata, rawdata)

    def _init_chg_status(self):
        self.status['chg'] = {
            'Description': "Chg Status register",
            'write': False,
            'TS': None,
            'data': None,
            'data_w': None
        }

    def update_chg_status(self):
        res = self.read_smbus(*self.REG_CHG_STATUS)
        self.status['chg']['TS'] = dt.now()
        self.status['chg']['data'] = self.Status_chg(res)

    # High level functions
    def update_mfr_info(self):
        try:
            mfr_info = [('ID', self.read_mfr_id(), dt.now()),
                        ('Model', self.read_mfr_model(), dt.now()),
                        ('Revision', self.read_mfr_revision(), dt.now()),
                        ('Location', self.read_mfr_location(), dt.now()),
                        ('Date', self.read_mfr_date(), dt.now()),
                        ('Serial', self.read_mfr_serial(), dt.now()),
                        ('PMBus Revision', self.read_pmbus_revision(), dt.now())]
            for key, value, now in mfr_info:
                self.mfr_info[key] = [value, now]
        except OSError as e:
            if e.errno == 121:
                print("Mfr info not completely updated")
            else:
                raise

    def get_mfr_info(self):
        return(self.mfr_info)

    def update_status(self):
        self.update_status_word()
        self.update_status_vout()
        self.update_status_iout()
        self.update_status_input()
        self.update_status_temperature()
        self.update_status_cml()
        self.update_status_mfr_specific()
        self.update_status_fans()
        self.update_chg_status()

    def get_status(self):
        return(self.status)

    def update_config(self):
        self.update_operation()
        self.update_on_off_config()
        self.update_capability()
        self.update_vout_mode()
        self.update_vout_command()
        self.update_vout_trim()
        self.update_iout_oc_fault_limit()
        self.update_iout_oc_fault_response()
        self.update_curve_cc_timeout()
        self.update_curve_cv_timeout()
        self.update_curve_ichg()
        self.update_curve_vbst()
        self.update_curve_vfloat()
        self.update_curve_itaper()
        self.update_curve_config()
        self.update_curve_float_timeout()

    def get_config(self):
        pass

    def set_config(self):
        pass

    def update_variables(self):
        self.update_vin()
        self.update_vout()
        self.update_iout()
        self.update_fan_speeds()

    def get_variables(self):
        pass


class ChargerCtrlState(Enum):
    Undefined = "undefined"
    ON = "on"
    CVM = "cvm"
    CCM = "ccm"
    OFF = "off"


class Rpb1600_charge_controller(object):
    def __init__(self, remote_pin=26, t_alarm_pin=13, dc_ok_pin=19,
                 start_trim=2.0, max_voltage=14.4*4, max_ccm_current=5.0,
                 trim_step=0.1):
        self.my_rpb = Mw_rpb1600(
            remote_pin=remote_pin, t_alarm_pin=t_alarm_pin, dc_ok_pin=dc_ok_pin)
        self.my_rpb.set_pin_data('cmded_state', Cmded_state.OFF)
        self.my_rpb.write_pin_data()
        self.my_rpb.set_operation(self.my_rpb.Operation.OFF)
        self.my_rpb.write_operation()
        self.my_rpb.update_config()
        self.my_rpb.update_status()
        self.my_rpb.update_pin_data()
        self.my_rpb.update_variables()
        print("Initial state")
        psprint(self.my_rpb)
        self.trim = start_trim
        self.max_voltage = max_voltage
        self.trim_max = self.max_voltage-48
        self.ccc = max_ccm_current
        self.my_rpb.set_vout_trim(self.trim)
        self.my_rpb.write_vout_trim()
        self.trim_gain = trim_step
        self.mode = ChargerCtrlState.Undefined

    def start(self):
        print("Starting charger operation")
        problem = True
        while problem:
            try:
                print("Set operation ON")
                self.my_rpb.set_operation(self.my_rpb.Operation.ON)
                print("Writing")
                self.my_rpb.write_operation()
                print("Set pin cmded state on")
                self.my_rpb.set_pin_data('cmded_state', Cmded_state.ON)
                print("Write pin")
                self.my_rpb.write_pin_data()
                self.my_rpb.update_pin_data()
                for i in range(2):
                    psprint(self.my_rpb)
            except OSError as e:
                print(f"Problems: {e}. Retrying")
            else:
                problem = False
                print("Operation started successfully")
                self.mode = ChargerCtrlState.ON
            sleep(1)

    def charge_loop(self):
        try:
            if self.mode == ChargerCtrlState.OFF:
                self.my_rpb.update_pin_data()
                psprint(self.my_rpb, endl='')
                sleep(0.01)
            else:
                print(f"Current trim: {self.trim:4.3f}", end=' ')
                self.my_rpb.set_vout_trim(self.trim)
                self.my_rpb.write_vout_trim()
                self.my_rpb.update_pin_data()
                psprint(self.my_rpb, endl='')
                sleep(0.01)
                self.cur = self.my_rpb.variables['iout']['data']
                self.error = self.ccc - self.cur
                print(f" Current error: {self.error:6.3f}", end=' ')
                print(f" Stage: ", end=' ')
                if self.error > 0:
                    self.trim += self.trim_gain
                elif self.error < 0:
                    self.trim -= self.trim_gain
                if self.trim > self.trim_max:
                    print("CVM", end='\r')
                    self.mode = ChargerCtrlState.CVM
                    self.trim = self.trim_max
                elif self.trim < -self.trim_max:
                    self.trim = -self.trim_max
                    self.mode = ChargerCtrlState.Undefined
                    print("Battery possibly damaged", end='\r')
                    # TODO important!!! Turn off power supply
                    raise RuntimeError
                else:
                    print("CCM", end='\r')
                    self.mode = ChargerCtrlState.CCM
                self.my_rpb.set_vout_trim(self.trim)
                self.my_rpb.write_vout_trim()
        except RuntimeError:
            print("Charging voltage too low. Maybe a battery problem")
            print("Stopping")
            return(False)
        except Exception as err:
            # TODO: capture exact communication Exceptions
            print("\nIgnore errors: ", err)
            return(True)
        else:
            return(True)

    def stop(self):
        print("Stopping charger")
        problem = True
        while problem:
            try:
                self.my_rpb.set_pin_data('cmded_state', Cmded_state.OFF)
                self.my_rpb.write_pin_data()
                self.my_rpb.set_operation(self.my_rpb.Operation.OFF)
                self.my_rpb.write_operation()
                for i in range(3):
                    psprint(self.my_rpb)
            except:
                print("Problems. Retrying")
            else:
                problem = False
                print("Operation stopped successfully")
                self.mode = ChargerCtrlState.OFF
            sleep(1)
