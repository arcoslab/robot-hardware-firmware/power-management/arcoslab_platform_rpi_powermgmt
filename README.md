# arcoslab_platform_rpi_powermgmt

Arcoslab Robot Platform RaspberryPi Power Management Python Module

Features:
- Provides access to a Meanwell RPB1600-48 power supply/battery charger through an I2C bus. 
- Provides control and monitor to simple power supplies (meanwell SD-500) that have RC (remote control) and Output_OK GPIO digital signals.
- Runs on a Raspberry PI 3 (care must be taken to assure that the GPIOs don't exceed 3.3V under no circunstances. With the meanwell power supplies use pull-down resistors to "bring down" the voltage)
