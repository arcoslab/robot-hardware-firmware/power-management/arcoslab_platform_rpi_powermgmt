#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c) 2021 Federico Ruiz-Ugalde
# Author: Federico Ruiz Ugalde <memeruiz at gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import arcoslab_platform_rpi_powermgmt.mw_rpb1600 as rpb
from time import sleep
from pprint import pprint as pp

my_rpb = rpb.Mw_rpb1600(remote_pin=13, t_alarm_pin=26, dc_ok_pin=19)
my_rpb.update_config()
my_rpb.update_status()
my_rpb.update_pin_data()
my_rpb.update_variables()

# Turns charging on and starts monitoring current, voltage, and pin data, until charge is finished
print("Pin State: ", list(my_rpb.pin_data.values())[1:])
print("Vin: ", my_rpb.variables['vin']['data'], "V")
print("Vout: ", my_rpb.variables['vout']['data'], "V")
print("Iout: ", my_rpb.variables['iout']['data'], "A")
print("2 or 3 stage charging")
stgs = my_rpb.config['curve_config']['data']['flags']
if stgs.value == 0:
    print("3 stage charging (Constant Current + Constant Voltage + Floating Voltage)")
else:
    print("2 stage charging (Constant Current + Constant Voltage)")
print("Constant Current configs:")
print("CC value: ", my_rpb.config['curve_ichg']['data'], "A")
print("Vboost: ", my_rpb.config['curve_vbst']['data'], "V")
print("Constant Voltage configs:")
print("Taper Current value: ", my_rpb.config['curve_itaper']['data'], "A")
print("Vfloat: ", my_rpb.config['curve_vfloat']['data'], "V")

print("Printing variable state")
done = False
count = 0
max_count = 100
turn_on_counter = 0
max_turn_on_counter = 100
while not done:
    my_rpb.update_status()
    my_rpb.update_variables()
    print(
        (
            f"Vin: {my_rpb.variables['vin']['data']:8.4f}V, "
            f"Vout: {my_rpb.variables['vout']['data']:7.4f}V, "
            f"Iout: {my_rpb.variables['iout']['data']:7.4f}A, "
            f"fan0: {my_rpb.variables['fans']['data'][0]:5d}rpm, "
            f"fan1: {my_rpb.variables['fans']['data'][0]:5d}rpm, "
            f"chg_stat: {my_rpb.status['chg']['data']:18}"
            f"chg_stat: {my_rpb.status['word']['data']}"
        ), end='\r'
    )
    if turn_on_counter == max_turn_on_counter:
        print("Starting change!!!")
        my_rpb.set_pin_data('cmded_state', rpb.Cmded_state.ON)
        my_rpb.write_pin_data()
        turn_on_counter = 1000
    elif turn_on_counter < max_turn_on_counter:
        turn_on_counter += 1

    if my_rpb.Status_chg.FULLM in my_rpb.status['chg']['data']:
        count += 1
    else:
        count = 0
    if count > max_count:
        done = True
    sleep(0.1)
