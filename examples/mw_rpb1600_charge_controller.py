#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c) 2021 Federico Ruiz-Ugalde
# Author: Federico Ruiz Ugalde <memeruiz at gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import arcoslab_platform_rpi_powermgmt.mw_rpb1600 as rpb
from arcoslab_platform_rpi_powermgmt.mw_rpb1600 import update_and_print_vars_stats as psprint
from time import sleep
from pprint import pprint as pp


class Rpb1600_charge_controller(object):
    def __init__(self):
        self.my_rpb = rpb.Mw_rpb1600(
            remote_pin=13, t_alarm_pin=26, dc_ok_pin=19)
        self.my_rpb.set_pin_data('cmded_state', rpb.Cmded_state.OFF)
        self.my_rpb.write_pin_data()
        self.my_rpb.set_operation(self.my_rpb.Operation.OFF)
        self.my_rpb.write_operation()
        self.my_rpb.update_config()
        self.my_rpb.update_status()
        self.my_rpb.update_pin_data()
        self.my_rpb.update_variables()
        print("Initial state")
        psprint(self.my_rpb)
        self.trim = 2.0
        self.max_voltage = 14.4*4
        self.trim_max = self.max_voltage-48
        self.ccc = 5.0
        self.my_rpb.set_vout_trim(self.trim)
        self.my_rpb.write_vout_trim()
        self.trim_gain = 0.1
        self.stop = False

    def charge_loop(self):
        print("Starting Charge")
        sleep(1)
        try:
            self.my_rpb.set_operation(self.my_rpb.Operation.ON)
            self.my_rpb.write_operation()
            self.my_rpb.set_pin_data('cmded_state', rpb.Cmded_state.ON)
            self.my_rpb.write_pin_data()
            self.my_rpb.update_pin_data()
            while not self.stop:
                try:
                    print(f"Current trim: {self.trim:4.3f}", end=' ')
                    self.my_rpb.set_vout_trim(self.trim)
                    self.my_rpb.write_vout_trim()
                    self.my_rpb.update_pin_data()
                    psprint(self.my_rpb, endl='')
                    sleep(0.01)
                    self.cur = self.my_rpb.variables['iout']['data']
                    self.error = self.ccc - self.cur
                    print(f" Current error: {self.error:6.3f}", end=' ')
                    print(f" Stage: ", end=' ')
                    if self.error > 0:
                        self.trim += self.trim_gain
                    elif self.error < 0:
                        self.trim -= self.trim_gain
                    if self.trim > self.trim_max:
                        print("CVM", end='\r')
                        self.trim = self.trim_max
                    elif self.trim < -self.trim_max:
                        self.trim = -self.trim_max
                        print("Battery possibly damaged", end='\r')
                        # TODO important!!! Turn off power supply
                        break
                    else:
                        print("CCM", end='\r')
                    self.my_rpb.set_vout_trim(self.trim)
                    self.my_rpb.write_vout_trim()
                except Exception as err:
                    print("\nIgnore errors: ", err)
                except KeyboardInterrupt:
                    raise
        except:
            print("problems")
        self.my_rpb.set_pin_data('cmded_state', rpb.Cmded_state.OFF)
        self.my_rpb.write_pin_data()
        self.my_rpb.set_operation(self.my_rpb.Operation.OFF)
        self.my_rpb.write_operation()
        for i in range(3):
            psprint(self.my_rpb)


# def try_charging(trimv, n=0, rep=5):
#     my_rpb.set_vout_trim(trimv)
#     my_rpb.write_vout_trim()
#     print(bin(my_rpb.update_vout_trim()))
#     print(my_rpb.config['vout_trim']['data_w'],
#           my_rpb.config['vout_trim']['data'])
#     try:
#         my_rpb.set_pin_data('cmded_state', rpb.Cmded_state.OFF)
#         my_rpb.write_pin_data()
#         my_rpb.set_operation(my_rpb.Operation.OFF)
#         my_rpb.write_operation()
#         for i in range(5):
#             psprint(my_rpb)
#         my_rpb.set_operation(my_rpb.Operation.ON)
#         my_rpb.write_operation()
#         my_rpb.set_pin_data('cmded_state', rpb.Cmded_state.ON)
#         my_rpb.write_pin_data()
#         my_rpb.update_pin_data()
#         print("Pin data: ", my_rpb.pin_data)
#         for i in range(rep):
#             try:
#                 psprint(my_rpb)
#             except Exception as err:
#                 print("Ignore errors: ", err)
#             except KeyboardInterrupt:
#                 raise
#     except:
#         print("problems")
#     my_rpb.set_pin_data('cmded_state', rpb.Cmded_state.OFF)
#     my_rpb.write_pin_data()
#     my_rpb.set_operation(my_rpb.Operation.OFF)
#     my_rpb.write_operation()
#     for i in range(3):
#         psprint(my_rpb)


if __name__ == "__main__":
    my_ctrl = Rpb1600_charge_controller()
    my_ctrl.charge_loop()
