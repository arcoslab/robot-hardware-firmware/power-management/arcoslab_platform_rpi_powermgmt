NAME=arcoslab_platform_rpi_powermgmt
PREFIX ?= ${HOME}/local/DIR/${NAME}
DEB_TARGET=python-arcoslab_platform_rpi_powermgmt_0.1-1_all.deb

all:
	echo "This Makefile will be removed in a future version"
	echo "Does nothing, try make install"

install:
	echo "This Makefile will be removed in a future version"
	mkdir -p ${HOME}/local/DIR/arcoslab_platform_rpi_powermgmt/lib/python2.7/site-packages/
	PYTHONPATH=${HOME}/local/DIR/arcoslab_platform_rpi_powermgmt/lib/python2.7/site-packages/ python setup.py install --prefix=${PREFIX}

install_py3:
	echo "This Makefile will be removed in a future version"
	PYTHONPATH=${HOME}/local/DIR/arcoslab_platform_rpi_powermgmt/lib/python3.6/site-packages/ python setup.py install --prefix=${PREFIX}

xstow_install: install
	echo "This Makefile will be removed in a future version"
	cd ${PREFIX}/../ && xstow ${NAME}

xstow_uninstall:
	echo "This Makefile will be removed in a future version"
	cd ${PREFIX}/../ && xstow -D ${NAME} && rm -rf ${NAME}

%.deb:
	echo "This Makefile will be removed in a future version"
	python setup.py --command-packages=stdeb.command bdist_deb

deb: deb_dist/${DEB_TARGET}
	echo "This Makefile will be removed in a future version"

deb_install: deb_dist/${DEB_TARGET}
	echo "This Makefile will be removed in a future version"
	cd deb_dist && sudo dpkg -i *.deb

clean:
	echo "This Makefile will be removed in a future version"
	python setup.py clean
	rm -rf build/ deb_dist/
